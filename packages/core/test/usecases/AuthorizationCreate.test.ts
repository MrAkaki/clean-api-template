import { AuthorizationCreateUsecase } from "../../src";

describe("UseCase [ AuthorizationCreate ]", () => {
  it("Should have current date in creation date", async () => {
    const mockUser = {
      id: "user-id",
      name: "username",
    };

    const configuration = {
      authorizationTime: 60,
    };
    const generateKeyRepository = {
      Execute: jest.fn().mockImplementation(() => {
        return "key-key-key";
      }),
    };

    const createAuthorizationRepository = {
      Execute: jest.fn().mockImplementation((autorization) => {
        return autorization;
      }),
    };

    const sut = new AuthorizationCreateUsecase(
      configuration,
      generateKeyRepository,
      createAuthorizationRepository
    );

    const auth = await sut.Execute(mockUser);
    const currentDate = new Date();
    expect(auth.creation.getFullYear()).toBe(currentDate.getFullYear());
    expect(auth.creation.getMonth()).toBe(currentDate.getMonth());
    expect(auth.creation.getDay()).toBe(currentDate.getDay());
    expect(auth.creation.getHours()).toBe(currentDate.getHours());
    expect(auth.creation.getMinutes()).toBe(currentDate.getMinutes());
    expect(auth.creation.getSeconds()).toBe(currentDate.getSeconds());
  });

  it("Should have date + authorizationTime in expiration date", async () => {
    const mockUser = {
      id: "user-id",
      name: "username",
    };

    const configuration = {
      authorizationTime: 60,
    };
    const generateKeyRepository = {
      Execute: jest.fn().mockImplementation(() => {
        return "key-key-key";
      }),
    };

    const createAuthorizationRepository = {
      Execute: jest.fn().mockImplementation((autorization) => {
        return autorization;
      }),
    };

    const sut = new AuthorizationCreateUsecase(
      configuration,
      generateKeyRepository,
      createAuthorizationRepository
    );

    const auth = await sut.Execute(mockUser);
  });
});
