import { User } from ".";

export interface Authorization {
  key: string;
  expiration: Date;
  creation: Date;
  belongsTo: string;
}
