import { Authorization } from "../entities";
import { Repository } from "../base";

export abstract class AuthorizationCreateRepository extends Repository<Authorization> {
  public abstract override Execute(user: Authorization): Promise<Authorization>;
}
