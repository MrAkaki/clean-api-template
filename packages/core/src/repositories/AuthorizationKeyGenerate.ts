import { Repository } from "../base";

export abstract class AuthorizationKeyGenerateRepository extends Repository<string> {
  public abstract override Execute(): string;
}
