import { Repository } from "../base";
import { User } from "../entities";

export abstract class UserFindByNameRepository extends Repository<User | null> {
  public abstract override Execute(name: string): Promise<User | null>;
}
