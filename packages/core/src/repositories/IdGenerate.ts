import { Repository } from "../base";

export abstract class IdGenerateRepository extends Repository<string> {
  public abstract override Execute(): string;
}
