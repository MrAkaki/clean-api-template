import { Authorization } from "../entities";
import { Repository } from "../base";

export abstract class AuthorizationFindByKeyRepository extends Repository<Authorization | null> {
  public abstract override Execute(key: string): Promise<Authorization | null>;
}
