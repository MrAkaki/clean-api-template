import { Repository } from "../base";
import { User } from "../entities";

export abstract class UserCreateRepository extends Repository<User> {
  public abstract override Execute(user: User): Promise<User>;
}
