import { Repository } from "../base";
import { User } from "../entities";

export abstract class UserFindByIdRepository extends Repository<User | null> {
  public abstract override Execute(id: string): Promise<User | null>;
}
