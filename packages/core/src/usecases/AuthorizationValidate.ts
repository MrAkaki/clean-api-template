import { AuthorizationFindByKeyRepository } from "../repositories";
import { Usecase } from "../base";
import { Authorization, User } from "../entities";

export abstract class AuthorizationValidateUsecaseDef extends Usecase<
  Promise<Authorization>
> {
  public abstract override Execute(authKey: string): Promise<Authorization>;
}

export class AuthorizationValidateUsecase
  implements AuthorizationValidateUsecaseDef
{
  public constructor(
    private findAuthorizationByKey: AuthorizationFindByKeyRepository
  ) {}

  public async Execute(key: string): Promise<Authorization> {
    const auth = await this.findAuthorizationByKey.Execute(key);
    if (auth == null) {
      throw new Error("This authorization not loger exist!");
    }
    if (auth.expiration > new Date()) {
      throw new Error("This authorization expired!");
    }
    return auth;
  }
}
