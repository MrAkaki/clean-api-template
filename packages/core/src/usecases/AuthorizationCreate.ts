import { AuthorizationKeyGenerateRepository } from "../repositories";
import { Usecase } from "../base";
import { Authorization, User } from "../entities";
import { AuthorizationCreateRepository } from "../repositories";

export type AuthorizationCreateUsecaseConfiguration = {
  authorizationTime: number;
};

export abstract class AuthorizationCreateUsecaseDef extends Usecase<
  Promise<Authorization>
> {
  public abstract override Execute(user: User): Promise<Authorization>;
}

export class AuthorizationCreateUsecase
  implements AuthorizationCreateUsecaseDef
{
  public constructor(
    private configuration: AuthorizationCreateUsecaseConfiguration,
    private generateKeyRepository: AuthorizationKeyGenerateRepository,
    private createAuthorizationRepository: AuthorizationCreateRepository
  ) {}

  public async Execute(user: User): Promise<Authorization> {
    const now = new Date();
    const expirationDate = new Date();
    expirationDate.setSeconds(
      expirationDate.getSeconds() + this.configuration.authorizationTime
    );
    const auth: Authorization = {
      key: this.generateKeyRepository.Execute(),
      expiration: expirationDate,
      creation: now,
      belongsTo: user.id,
    };

    await this.createAuthorizationRepository.Execute(auth);

    return auth;
  }
}
