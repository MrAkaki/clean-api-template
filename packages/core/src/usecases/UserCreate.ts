import {
  UserFindByNameRepository,
  IdGenerateRepository,
  UserCreateRepository,
} from "../repositories";

import { Usecase } from "../base";

import { User } from "../entities";

export abstract class UserCreateUsecaseDef extends Usecase<Promise<User>> {
  public abstract override Execute(user: Partial<User>): Promise<User>;
}

export class UserCreateUsecase implements UserCreateUsecaseDef {
  public constructor(
    private generateIdRepository: IdGenerateRepository,
    private createUserRepository: UserCreateRepository,
    private findUserRepository: UserFindByNameRepository
  ) {}

  public async Execute(user: Partial<User>): Promise<User> {
    // TODO: validate user name
    const fUser = await this.findUserRepository.Execute(user.name);
    if (fUser != null) {
      throw new Error(
        "An user with this name is already registered in our system!"
      );
    }
    const nUser: User = {
      id: this.generateIdRepository.Execute(),
      name: user.name,
    };
    return await this.createUserRepository.Execute(nUser);
  }
}
