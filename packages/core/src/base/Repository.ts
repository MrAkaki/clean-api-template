export abstract class Repository<O> {
  public abstract Execute(...args: any[]): O | Promise<O>;
}
