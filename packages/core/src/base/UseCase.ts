export abstract class Usecase<T> {
  public abstract Execute(...args: any[]): T | Promise<T>;
}
