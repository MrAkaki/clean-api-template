import { Usecase } from "../base";
export class AuthorizationValidateUsecaseDef extends Usecase {
}
export class AuthorizationValidateUsecase {
    constructor(findAuthorizationByKey) {
        this.findAuthorizationByKey = findAuthorizationByKey;
    }
    async Execute(key) {
        const auth = await this.findAuthorizationByKey.Execute(key);
        if (auth == null) {
            throw new Error("This authorization not loger exist!");
        }
        if (auth.expiration > new Date()) {
            throw new Error("This authorization expired!");
        }
        return auth;
    }
}
//# sourceMappingURL=AuthorizationValidate.js.map