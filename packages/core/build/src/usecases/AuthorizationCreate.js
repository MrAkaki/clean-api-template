import { Usecase } from "../base";
export class AuthorizationCreateUsecaseDef extends Usecase {
}
export class AuthorizationCreateUsecase {
    constructor(configuration, generateKeyRepository, createAuthorizationRepository) {
        this.configuration = configuration;
        this.generateKeyRepository = generateKeyRepository;
        this.createAuthorizationRepository = createAuthorizationRepository;
    }
    async Execute(user) {
        const now = new Date();
        const expirationDate = new Date();
        expirationDate.setSeconds(expirationDate.getSeconds() + this.configuration.authorizationTime);
        const auth = {
            key: this.generateKeyRepository.Execute(),
            expiration: expirationDate,
            creation: now,
            belongsTo: user.id,
        };
        await this.createAuthorizationRepository.Execute(auth);
        return auth;
    }
}
//# sourceMappingURL=AuthorizationCreate.js.map