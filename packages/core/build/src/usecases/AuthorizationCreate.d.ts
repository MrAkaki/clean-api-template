import { AuthorizationKeyGenerateRepository } from "../repositories";
import { Usecase } from "../base";
import { Authorization, User } from "../entities";
import { AuthorizationCreateRepository } from "../repositories";
export type AuthorizationCreateUsecaseConfiguration = {
    authorizationTime: number;
};
export declare abstract class AuthorizationCreateUsecaseDef extends Usecase<Promise<Authorization>> {
    abstract Execute(user: User): Promise<Authorization>;
}
export declare class AuthorizationCreateUsecase implements AuthorizationCreateUsecaseDef {
    private configuration;
    private generateKeyRepository;
    private createAuthorizationRepository;
    constructor(configuration: AuthorizationCreateUsecaseConfiguration, generateKeyRepository: AuthorizationKeyGenerateRepository, createAuthorizationRepository: AuthorizationCreateRepository);
    Execute(user: User): Promise<Authorization>;
}
//# sourceMappingURL=AuthorizationCreate.d.ts.map