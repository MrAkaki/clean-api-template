import { Usecase } from "../base";
export class UserCreateUsecaseDef extends Usecase {
}
export class UserCreateUsecase {
    constructor(generateIdRepository, createUserRepository, findUserRepository) {
        this.generateIdRepository = generateIdRepository;
        this.createUserRepository = createUserRepository;
        this.findUserRepository = findUserRepository;
    }
    async Execute(user) {
        // TODO: validate user name
        const fUser = await this.findUserRepository.Execute(user.name);
        if (fUser != null) {
            throw new Error("An user with this name is already registered in our system!");
        }
        const nUser = {
            id: this.generateIdRepository.Execute(),
            name: user.name,
        };
        return await this.createUserRepository.Execute(nUser);
    }
}
//# sourceMappingURL=UserCreate.js.map