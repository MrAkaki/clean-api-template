declare abstract class Repository<O> {
    abstract Execute(...args: any[]): O | Promise<O>;
}

declare abstract class Usecase<T> {
    abstract Execute(...args: any[]): T | Promise<T>;
}

interface Authorization {
    key: string;
    expiration: Date;
    creation: Date;
    belongsTo: string;
}

interface User {
    id: string;
    name: string;
}

declare abstract class AuthorizationKeyGenerateRepository extends Repository<string> {
    abstract Execute(): string;
}

declare abstract class AuthorizationFindByKeyRepository extends Repository<Authorization | null> {
    abstract Execute(key: string): Promise<Authorization | null>;
}

declare abstract class AuthorizationCreateRepository extends Repository<Authorization> {
    abstract Execute(user: Authorization): Promise<Authorization>;
}

declare abstract class IdGenerateRepository extends Repository<string> {
    abstract Execute(): string;
}

declare abstract class UserCreateRepository extends Repository<User> {
    abstract Execute(user: User): Promise<User>;
}

declare abstract class UserFindByNameRepository extends Repository<User | null> {
    abstract Execute(name: string): Promise<User | null>;
}

declare abstract class UserFindByIdRepository extends Repository<User | null> {
    abstract Execute(id: string): Promise<User | null>;
}

declare abstract class UserCreateUsecaseDef extends Usecase<Promise<User>> {
    abstract Execute(user: Partial<User>): Promise<User>;
}
declare class UserCreateUsecase implements UserCreateUsecaseDef {
    private generateIdRepository;
    private createUserRepository;
    private findUserRepository;
    constructor(generateIdRepository: IdGenerateRepository, createUserRepository: UserCreateRepository, findUserRepository: UserFindByNameRepository);
    Execute(user: Partial<User>): Promise<User>;
}

type AuthorizationCreateUsecaseConfiguration = {
    authorizationTime: number;
};
declare abstract class AuthorizationCreateUsecaseDef extends Usecase<Promise<Authorization>> {
    abstract Execute(user: User): Promise<Authorization>;
}
declare class AuthorizationCreateUsecase implements AuthorizationCreateUsecaseDef {
    private configuration;
    private generateKeyRepository;
    private createAuthorizationRepository;
    constructor(configuration: AuthorizationCreateUsecaseConfiguration, generateKeyRepository: AuthorizationKeyGenerateRepository, createAuthorizationRepository: AuthorizationCreateRepository);
    Execute(user: User): Promise<Authorization>;
}

export { Authorization, AuthorizationCreateRepository, AuthorizationCreateUsecase, AuthorizationCreateUsecaseConfiguration, AuthorizationCreateUsecaseDef, AuthorizationFindByKeyRepository, AuthorizationKeyGenerateRepository, IdGenerateRepository, Repository, Usecase, User, UserCreateRepository, UserCreateUsecase, UserCreateUsecaseDef, UserFindByIdRepository, UserFindByNameRepository };
