class Repository {
}

class Usecase {
}

class AuthorizationKeyGenerateRepository extends Repository {
}

class AuthorizationFindByKeyRepository extends Repository {
}

class AuthorizationCreateRepository extends Repository {
}

class IdGenerateRepository extends Repository {
}

class UserCreateRepository extends Repository {
}

class UserFindByNameRepository extends Repository {
}

class UserFindByIdRepository extends Repository {
}

class UserCreateUsecaseDef extends Usecase {
}
class UserCreateUsecase {
    constructor(generateIdRepository, createUserRepository, findUserRepository) {
        this.generateIdRepository = generateIdRepository;
        this.createUserRepository = createUserRepository;
        this.findUserRepository = findUserRepository;
    }
    async Execute(user) {
        // TODO: validate user name
        const fUser = await this.findUserRepository.Execute(user.name);
        if (fUser != null) {
            throw new Error("An user with this name is already registered in our system!");
        }
        const nUser = {
            id: this.generateIdRepository.Execute(),
            name: user.name,
        };
        return await this.createUserRepository.Execute(nUser);
    }
}

class AuthorizationCreateUsecaseDef extends Usecase {
}
class AuthorizationCreateUsecase {
    constructor(configuration, generateKeyRepository, createAuthorizationRepository) {
        this.configuration = configuration;
        this.generateKeyRepository = generateKeyRepository;
        this.createAuthorizationRepository = createAuthorizationRepository;
    }
    async Execute(user) {
        const now = new Date();
        const expirationDate = new Date();
        expirationDate.setSeconds(expirationDate.getSeconds() + this.configuration.authorizationTime);
        const auth = {
            key: this.generateKeyRepository.Execute(),
            expiration: expirationDate,
            creation: now,
            belongsTo: user.id,
        };
        await this.createAuthorizationRepository.Execute(auth);
        return auth;
    }
}

export { AuthorizationCreateRepository, AuthorizationCreateUsecase, AuthorizationCreateUsecaseDef, AuthorizationFindByKeyRepository, AuthorizationKeyGenerateRepository, IdGenerateRepository, Repository, Usecase, UserCreateRepository, UserCreateUsecase, UserCreateUsecaseDef, UserFindByIdRepository, UserFindByNameRepository };
//# sourceMappingURL=index.js.map
