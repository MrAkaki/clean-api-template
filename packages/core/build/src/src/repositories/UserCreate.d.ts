import { Repository } from "../base";
import { User } from "../entities";
export declare abstract class UserCreateRepository extends Repository<User> {
    abstract Execute(user: User): Promise<User>;
}
//# sourceMappingURL=UserCreate.d.ts.map