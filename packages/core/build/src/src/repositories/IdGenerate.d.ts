import { Repository } from "../base";
export declare abstract class IdGenerateRepository extends Repository<string> {
    abstract Execute(): string;
}
//# sourceMappingURL=IdGenerate.d.ts.map