import { Repository } from "../base";
import { User } from "../entities";
export declare abstract class UserFindByIdRepository extends Repository<User | null> {
    abstract Execute(id: string): Promise<User | null>;
}
//# sourceMappingURL=UserFindById.d.ts.map