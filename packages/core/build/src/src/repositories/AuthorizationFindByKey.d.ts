import { Authorization } from "../entities";
import { Repository } from "../base";
export declare abstract class AuthorizationFindByKeyRepository extends Repository<Authorization | null> {
    abstract Execute(key: string): Promise<Authorization | null>;
}
//# sourceMappingURL=AuthorizationFindByKey.d.ts.map