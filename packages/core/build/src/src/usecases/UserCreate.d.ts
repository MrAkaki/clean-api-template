import { UserFindByNameRepository, IdGenerateRepository, UserCreateRepository } from "../repositories";
import { Usecase } from "../base";
import { User } from "../entities";
export declare abstract class UserCreateUsecaseDef extends Usecase<Promise<User>> {
    abstract Execute(user: Partial<User>): Promise<User>;
}
export declare class UserCreateUsecase implements UserCreateUsecaseDef {
    private generateIdRepository;
    private createUserRepository;
    private findUserRepository;
    constructor(generateIdRepository: IdGenerateRepository, createUserRepository: UserCreateRepository, findUserRepository: UserFindByNameRepository);
    Execute(user: Partial<User>): Promise<User>;
}
//# sourceMappingURL=UserCreate.d.ts.map