import { AuthorizationFindByKeyRepository } from "../repositories";
import { Usecase } from "../base";
import { Authorization } from "../entities";
export declare abstract class AuthorizationValidateUsecaseDef extends Usecase<Promise<Authorization>> {
    abstract Execute(authKey: string): Promise<Authorization>;
}
export declare class AuthorizationValidateUsecase implements AuthorizationValidateUsecaseDef {
    private findAuthorizationByKey;
    constructor(findAuthorizationByKey: AuthorizationFindByKeyRepository);
    Execute(key: string): Promise<Authorization>;
}
//# sourceMappingURL=AuthorizationValidate.d.ts.map