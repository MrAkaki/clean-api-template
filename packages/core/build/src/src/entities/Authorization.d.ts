export interface Authorization {
    key: string;
    expiration: Date;
    creation: Date;
    belongsTo: string;
}
//# sourceMappingURL=Authorization.d.ts.map