export declare abstract class Usecase<T> {
    abstract Execute(...args: any[]): T | Promise<T>;
}
//# sourceMappingURL=UseCase.d.ts.map