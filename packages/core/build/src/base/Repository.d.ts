export declare abstract class Repository<O> {
    abstract Execute(...args: any[]): O | Promise<O>;
}
//# sourceMappingURL=Repository.d.ts.map