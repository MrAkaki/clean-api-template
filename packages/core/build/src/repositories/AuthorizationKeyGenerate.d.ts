import { Repository } from "../base";
export declare abstract class AuthorizationKeyGenerateRepository extends Repository<string> {
    abstract Execute(): string;
}
//# sourceMappingURL=AuthorizationKeyGenerate.d.ts.map