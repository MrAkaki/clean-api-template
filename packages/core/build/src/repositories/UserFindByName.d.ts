import { Repository } from "../base";
import { User } from "../entities";
export declare abstract class UserFindByNameRepository extends Repository<User | null> {
    abstract Execute(name: string): Promise<User | null>;
}
//# sourceMappingURL=UserFindByName.d.ts.map