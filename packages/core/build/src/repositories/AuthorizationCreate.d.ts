import { Authorization } from "../entities";
import { Repository } from "../base";
export declare abstract class AuthorizationCreateRepository extends Repository<Authorization> {
    abstract Execute(user: Authorization): Promise<Authorization>;
}
//# sourceMappingURL=AuthorizationCreate.d.ts.map