import { UserCreateUsecase } from "../../src";
describe("UseCase [ UserCreate ]", () => {
    it("Should throw if user already registered", async () => {
        const generateIdRepository = {
            Execute: jest.fn().mockImplementation(() => {
                return "aaa";
            }),
        };
        const createUserRepository = {
            Execute: jest.fn().mockImplementation(() => {
                return { id: "aaa", name: "name" };
            }),
        };
        const findUserRepositoryExist = {
            Execute: jest.fn().mockImplementation(() => {
                return { id: "aaa", name: "name" };
            }),
        };
        const sut = new UserCreateUsecase(generateIdRepository, createUserRepository, findUserRepositoryExist);
        expect.assertions(4);
        try {
            await sut.Execute({ name: "username" });
        }
        catch (e) {
            expect(e.message).toMatch("An user with this name is already registered in our system!");
        }
        expect(findUserRepositoryExist.Execute).toHaveBeenCalledTimes(1);
        expect(createUserRepository.Execute).toHaveBeenCalledTimes(0);
        expect(generateIdRepository.Execute).toHaveBeenCalledTimes(0);
    });
    it("Should call IdGenerateRepository,UserCreateRepository", async () => {
        const mockUser = { id: "aaa", name: "name" };
        const generateIdRepository = {
            Execute: jest.fn().mockImplementation(() => {
                return mockUser.id;
            }),
        };
        const createUserRepository = {
            Execute: jest.fn().mockImplementation(() => {
                return mockUser;
            }),
        };
        const findUserRepositoryExist = {
            Execute: jest.fn().mockImplementation(() => {
                return null;
            }),
        };
        const sut = new UserCreateUsecase(generateIdRepository, createUserRepository, findUserRepositoryExist);
        expect.assertions(4);
        expect(await sut.Execute({ name: "username" })).toBe(mockUser);
        expect(findUserRepositoryExist.Execute).toHaveBeenCalledTimes(1);
        expect(createUserRepository.Execute).toHaveBeenCalledTimes(1);
        expect(generateIdRepository.Execute).toHaveBeenCalledTimes(1);
    });
});
//# sourceMappingURL=UserCreate.test.js.map