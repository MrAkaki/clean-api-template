FROM node:18

WORKDIR /app

COPY package*.json ./

RUN npm install --production

COPY . .

RUN npm run prestart
RUN npm run build

CMD [ "node", "build/index.ts" ]